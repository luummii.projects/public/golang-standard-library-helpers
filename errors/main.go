package main

import (
	"errors"
	"log"
	"time"
)

// MyError is an error implementation that includes a time and message.
type MyError struct {
	When time.Time
	What string
}

// MyError now implicitly implememts the error interface
func (e MyError) Error() string {
	log.Printf("%v: %v", e.When, e.What)
	return string("Error: la la la")
}

// OopsieDaisies returns an error, in this case a MyError error since
// it implements from the error interface. If it didn't, returning
// the &MyError object would not be valid to return as it would
// not be classified as an error without implementing the error interface
func OopsieDaisies() error {
	return &MyError{
		time.Now(),
		"There has been a woopsie dasies error.",
	}
}

func slurpy(hasSlurpy bool) error {
	if hasSlurpy {
		return nil
	}
	return errors.New("Error: No slurpy found")
}

func errorReturningFunction() error {
	return errors.New("Error: ErrorReturningFunction")
}

func main() {
	if err := OopsieDaisies(); err != nil {
		log.Println("sss", err)
	}

	if err := slurpy(false); err != nil {
		log.Println(err)
	}

	if err := errorReturningFunction(); err != nil {
		log.Println(err)
	}
}
