package main

import (
	"log"
	"syscall"
)

func main() {
	statfs := new(syscall.Statfs_t)
	if err := syscall.Statfs("/Users/Benjamin", statfs); err != nil {
		log.Fatalln(err)
	}

	log.Println(statfs.Bfree)
	log.Println(statfs.Owner)
}
