// The cap built-in function returns the capacity of v,
// according to its type
// The len built-in function returns the length of v,
// according to its type:
package main

import (
	"log"
	"reflect"
)

func main() {
	// String len: the number of bytes in v
	log.Println("Strings:")

	log.Println("\nString One:")
	str := "Hello, World!"
	log.Println(str)
	log.Println("len:", len(str)) // 13

	log.Println()

	// Array cap: the number of elements in v (same as len(v)).
	// Array len: the number of elements in v.
	log.Println("Arrays:")

	log.Println("\nArray One:")
	var arr_one [3]int
	log.Println(arr_one)
	log.Println("cap:", cap(arr_one)) // 3
	log.Println("len:", len(arr_one)) // 3

	log.Println()

	// Pointer to array cap: the number of elements in *v (same as len(v)).
	// Pointer to array len: the number of elements in *v (even if v is nil).
	log.Println("Pointers:")
	ptr := new([]int)
	*ptr = []int{1, 2}
	log.Println(ptr)
	log.Println("cap:", cap(*ptr)) // 2
	log.Println("len:", len(*ptr)) // 2

	log.Println()

	// Slice cap: the maximum length the slice can reach when resliced;
	// Slice & Map len: the number of elements in v
	log.Println("Slices:")

	log.Println("\nSlice One:")
	slice_one := []int{1, 2, 3}
	log.Println(slice_one)
	log.Println("cap:", cap(slice_one)) // 3
	log.Println("len:", len(slice_one)) // 3

	log.Println("\nSlice Two:")
	slice_two := [10]int{1, 2, 3}
	log.Println(slice_two)
	log.Println("cap:", cap(slice_two)) // 10
	log.Println("len:", len(slice_two)) // 10

	log.Println("\nSlice Three:")
	slice_three := make([]int, 0, 5)
	log.Println(slice_three)
	log.Println("cap:", cap(slice_three)) // 5
	log.Println("len:", len(slice_three)) // 0

	log.Println("\nSlice Four:")
	slice_four := slice_three[:2]
	log.Println(slice_four)
	log.Println("cap:", cap(slice_four)) // 5
	log.Println("len:", len(slice_four)) // 2

	log.Println("\nSlice Five:")
	slice_five := slice_four[2:5]
	log.Println(slice_five)
	log.Println("cap:", cap(slice_five)) // 3
	log.Println("len:", len(slice_five)) // 3

	log.Println()

	// Channel cap: the channel buffer capacity, in units of elements;
	// Channel len: the number of elements queued (unread) in the channel buffer
	log.Println("Channels:")

	log.Println("\nChannel Two:")
	channel_one := make(chan int, 5)
	log.Println(reflect.TypeOf(channel_one))
	log.Println("cap:", cap(channel_one)) // 5
	log.Println("len:", len(channel_one)) // 0
	close(channel_one)

	log.Println("\nChannel Two:")
	channel_two := make(chan int, 5)
	channel_two <- 1
	channel_two <- 2
	log.Println(reflect.TypeOf(channel_two))
	log.Println("cap:", cap(channel_two)) // 5
	log.Println("len:", len(channel_two)) // 2
	close(channel_two)
}
