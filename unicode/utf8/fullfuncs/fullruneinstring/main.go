package main

import (
	"log"
	"unicode/utf8"
)

func main() {
	str := "世"

	// FullRuneInString is like FullRune but its input is a string.
	log.Println(utf8.FullRuneInString(str))
	log.Println(utf8.FullRuneInString(str[:2]))
}
