package main

import (
	"log"
	"unicode/utf8"
)

func main() {
	str := "Hello, 世界"
	log.Println("bytes =", len(str))

	// RuneCountInString is like RuneCount but its input is a string.
	log.Println("runes =", utf8.RuneCountInString(str))
}
