package main

import (
	"log"
	"unicode/utf8"
)

func main() {
	buf := []byte("Hello, 世界")
	log.Println("bytes =", len(buf))

	// RuneCount returns the number of runes in p.  Erroneous and short
	// encodings are treated as single runes of width 1 byte.
	log.Println("runes =", utf8.RuneCount(buf))
}
