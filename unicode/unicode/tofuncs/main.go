package main

import (
	"log"
	"unicode"
)

func main() {
	const ucG = 'G'

	// ToLower maps the rune to lower case.
	log.Printf("%#U\n", unicode.ToLower(ucG))

	const lcG = 'g'

	// ToTitle maps the rune to title case.
	log.Printf("%#U\n", unicode.ToTitle(lcG))

	// ToUpper maps the rune to upper case.
	log.Printf("%#U\n", unicode.ToUpper(lcG))
}
