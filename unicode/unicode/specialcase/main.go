package main

import (
	"log"
	"unicode"
)

func main() {
	t := unicode.TurkishCase

	const lci = 'i'

	// ToLower maps the rune to lower case giving priority to the special mapping.
	log.Printf("%#U\n", t.ToLower(lci))

	// ToTitle maps the rune to title case giving priority to the special mapping.
	log.Printf("%#U\n", t.ToTitle(lci))

	// ToUpper maps the rune to upper case giving priority to the special mapping.
	log.Printf("%#U\n", t.ToUpper(lci))

	const uci = 'İ'
	log.Printf("%#U\n", t.ToLower(uci))
	log.Printf("%#U\n", t.ToTitle(uci))
	log.Printf("%#U\n", t.ToUpper(uci))
}
