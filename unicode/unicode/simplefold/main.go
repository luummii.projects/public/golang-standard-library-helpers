package main

import (
	"log"
	"unicode"
)

func main() {
	// SimpleFold iterates over Unicode code points equivalent under
	// the Unicode-defined simple case folding.  Among the code points
	// equivalent to rune (including rune itself), SimpleFold returns the
	// smallest rune > r if one exists, or else the smallest rune >= 0.
	//
	// For example:
	//	SimpleFold('A') = 'a'
	//	SimpleFold('a') = 'A'
	//
	//	SimpleFold('K') = 'k'
	//	SimpleFold('k') = '\u212A' (Kelvin symbol, K)
	//	SimpleFold('\u212A') = 'K'
	//
	//	SimpleFold('1') = '1'
	sr := unicode.SimpleFold('\u212A')
	log.Println(sr) // wrong way to print!

	log.Printf("%+q\n", sr) // simpleFold will return K (Kelvin symbol)

	src := unicode.SimpleFold('你')
	log.Printf("%q\n", src)  // simpleFold will return the same character 你
	log.Printf("%+q\n", src) // simpleFold will return the unicode codepoint

	sra := unicode.SimpleFold('A')
	log.Printf("%+q\n", sra) // simpleFold will return the lower case 'a'

	log.Println()
	log.Printf("%#U\n", unicode.SimpleFold('A'))      // 'a'
	log.Printf("%#U\n", unicode.SimpleFold('a'))      // 'A'
	log.Printf("%#U\n", unicode.SimpleFold('K'))      // 'k'
	log.Printf("%#U\n", unicode.SimpleFold('k'))      // '\u212A' (Kelvin symbol, K)
	log.Printf("%#U\n", unicode.SimpleFold('\u212A')) // 'K'
	log.Printf("%#U\n", unicode.SimpleFold('1'))      // '1
}
