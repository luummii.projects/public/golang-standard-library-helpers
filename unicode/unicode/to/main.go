package main

import (
	"log"
	"unicode"
)

func main() {
	const lcG = 'g'

	// To maps the rune to the specified case: UpperCase, LowerCase, or TitleCase.
	log.Printf("%#U\n", unicode.To(unicode.UpperCase, lcG))
	log.Printf("%#U\n", unicode.To(unicode.LowerCase, lcG))
	log.Printf("%#U\n", unicode.To(unicode.TitleCase, lcG))

	const ucG = 'G'
	log.Printf("%#U\n", unicode.To(unicode.UpperCase, ucG))
	log.Printf("%#U\n", unicode.To(unicode.LowerCase, ucG))
	log.Printf("%#U\n", unicode.To(unicode.TitleCase, ucG))
}
