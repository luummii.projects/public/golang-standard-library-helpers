package main

import (
	"log"
	"unicode/utf16"
)

func main() {
	rune_array := []rune{'a', 'b', '好', 0xfffd}

	var uint16_array []uint16

	// Encode returns the UTF-16 encoding of the Unicode code point sequence s.
	uint16_array = utf16.Encode(rune_array)

	log.Println(uint16_array)

	// 0xfffd should not be encoded.

	for _, item := range uint16_array {
		log.Printf("0x%x \n", item)
	}
}
