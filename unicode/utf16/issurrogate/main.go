package main

import (
	"log"
	"unicode/utf16"
)

func main() {
	// IsSurrogate reports whether the specified Unicode code point
	// can appear in a surrogate pair.
	answer := utf16.IsSurrogate('水')
	log.Println(answer)

	answer = utf16.IsSurrogate('\U0001D11E')
	log.Println(answer)

	answer = utf16.IsSurrogate(rune(0xdc00))
	log.Println(answer)

	answer = utf16.IsSurrogate('\u6C34')
	log.Println(answer)

	answer = utf16.IsSurrogate(rune(0xdfff))
	log.Println(answer)
}
