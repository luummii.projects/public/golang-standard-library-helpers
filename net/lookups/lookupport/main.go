package main

import (
	"log"
	"net"
)

func main() {
	// Look up the port for the network type tcp and the service telnet
	//
	// LookupPort looks up the port for the given network and service.
	port, err := net.LookupPort("tcp", "telnet")
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(port)
}
