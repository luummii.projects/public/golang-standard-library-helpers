package main

import (
	"log"
	"time"
)

func main() {
	year, month, day := time.Now().Date()
	log.Printf("Year: %v\nMonth: %v\nDay: %v\n", year, month, day)
}
