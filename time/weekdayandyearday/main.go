package main

import (
	"log"
	"time"
)

func main() {
	// Get the current time and store it in `t`
	t := time.Now()

	// Weekday returns the day of the week specified by t.
	log.Println("Weekday:", t.Weekday())

	// YearDay returns the day of the year specified by t, in the
	// range [1,365] for non-leap years, and [1,366] in leap years.
	log.Println("Day of the year:", t.YearDay())
}
