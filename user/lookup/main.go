package main

import (
	"log"
	"os/user"
)

func main() {
	// Lookup a user on the computer by username
	//
	// Lookup looks up a user by username. If the user cannot be found,
	// the returned error is of type UnknownUserError.
	u, err := user.Lookup("zimin6077@gmail.com") // On Windows - emeil is user name
	if err != nil {
		log.Fatalln(err)
	}

	// Uid is the user ID.
	// On POSIX systems, this is a decimal number representing the uid.
	// On Windows, this is a security identifier (SID) in a string format.
	log.Printf("Uid: %s", u.Uid)

	// Gid is the primary group ID.
	// On POSIX systems, this is a decimal number representing the gid.
	// On Windows, this is a SID in a string format.
	log.Printf("Gid: %s", u.Gid)

	// Username is the login name.
	log.Printf("Username: %s", u.Username)

	// Name is the user's real or display name.
	// It might be blank.
	// On POSIX systems, this is the first (or only) entry in the GECOS field
	// On Windows, this is the user's display name.
	log.Printf("Name: %s", u.Name)

	// HomeDir is the path to the user's home directory (if they have one).
	log.Printf("HomeDir: %s", u.HomeDir)
}
