package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"net/textproto"
	"os"
	"strings"
)

func main() {
	ln, err := net.Listen("tcp", "localhost:9000")
	if err != nil {
		log.Fatalln(err)
	}
	defer ln.Close()

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatalln(err)
		}

		go handleConnections(conn)
	}
}

func handleConnections(conn net.Conn) {
	defer conn.Close()

	mw := io.MultiWriter(os.Stdout, conn)

	tp := textproto.NewReader(bufio.NewReader(conn))
	code := 100
	for {
		if found, msg := readCode(code, tp); !found {
			continue
		} else {
			_, err := io.Copy(mw, strings.NewReader(msg))
			if err != nil {
				log.Fatalln(err)
			}
		}
		code += 200
	}
}

func readCode(codeInt int, tp *textproto.Reader) (bool, string) {
	code, msg, err := tp.ReadCodeLine(codeInt)
	if err != nil {
		if err != io.EOF {
			return false, ""
		}
	} else {
		msg = fmt.Sprintf("%d - %s\n", code, msg)
	}
	return true, msg
}
