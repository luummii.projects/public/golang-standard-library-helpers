package main

import (
	"bufio"
	"bytes"
	"log"
	"net/textproto"
)

func main() {
	readBuffer := bytes.NewBuffer([]byte("200 Command Okay.\n"))
	reader := *bufio.NewReader(readBuffer)
	tpReader := textproto.NewReader(&reader)

	code, msg, err := tpReader.ReadResponse(200)
	if err != nil {
		log.Println("Read error:", err)
	}

	log.Println("Code:", code)
	log.Println("Message:", msg)
}
