package main

import (
	"log"
	"net/url"
)

func main() {
	// Parse a url
	u, err := url.Parse("http://localhost:9000/search name?name=Benjamin Radovsky")
	if err != nil {
		log.Fatalln(err)
	}

	// Print out the url's non escaped path.
	log.Printf("Path: %s", u.Path) // search name

	log.Printf("EscapedPath: %s", u.EscapedPath()) // search%20name

	// Print out whether the url is absolute or not.
	log.Printf("IsAbs: %v", u.IsAbs())

	log.Println("--------------------")

	// Parse another url.
	u, err = url.Parse("localhost?name=Benjamin")
	if err != nil {
		log.Fatalln(err)
	}

	// Print out whether the url is absolute or not.
	log.Printf("IsAbs: %v", u.IsAbs())
}
