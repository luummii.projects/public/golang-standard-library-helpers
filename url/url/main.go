package main

import (
	"log"
	"net/url"
)

func main() {
	// Parse a url.
	//
	// Parse parses rawurl into a URL structure.
	// The rawurl may be relative or absolute.
	u, err := url.Parse("https://example.com/foo%2fbar#im_a_fragment")
	if err != nil {
		log.Fatal(err)
	}

	// Print out the url's path.
	// Prints /foo/bar even though the url was encoded as foo%2fbar.
	log.Printf("Path: %s", u.Path)

	// Print out the url's raw path.
	log.Printf("RawPath: %s", u.RawPath)

	// Print out the url's fragment.
	log.Printf("Fragment: %s", u.Fragment)

	// Print out the url as a string.
	log.Printf("String: %s", u.String())

	// Print a blank line before the next url's prints.
	log.Println("--------------------")

	// Parse a url.
	u, err = url.Parse("http://localhost:9000/search?name=Benjamin Radovsky")
	if err != nil {
		log.Fatalln(err)
	}

	// Print out the url's scheme.
	log.Printf("Scheme: %s", u.Scheme)

	// Print out the url's host.
	log.Printf("Host: %s", u.Host)

	// Print out the url's path.
	log.Printf("Path: %s", u.Path)

	// Print out the url's raw query.
	log.Printf("RawQuery: %s", u.RawQuery)

	// Encode and then print out the encoded query.
	//
	// Query parses RawQuery and returns the corresponding values.
	//
	// Encode encodes the values into ``URL encoded'' form
	// ("bar=baz&foo=quux") sorted by key.
	log.Printf("Encode: %s", u.Query().Encode())

	// Print out the whole url.
	log.Printf("u: %s", u)
}
