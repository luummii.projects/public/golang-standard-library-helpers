package main

import (
	"log"
	"net/url"
)

func main() {
	// username and password must be in plain text (see below).
	rawURL := "http://localhost:9000/search/?q=Benjamin#BenjiFragment#Luummii"

	val, err := url.ParseQuery(rawURL)
	if err != nil {
		log.Fatalln(err)
	}

	// Print out the raw query key and value.
	for k, v := range val {
		log.Printf("1 %v:\n 2 %v\n", k, v)
	}
}
