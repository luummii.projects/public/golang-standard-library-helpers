package main

import (
	"log"
	"net/url"
)

func main() {
	// Parse a partial non-absolute url.
	u, err := url.Parse("../../../search?q=gophers")
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("IsAbs: %s", u.IsAbs())

	// Parse an absolute base url.
	base, err := url.Parse("http://example.com/one/two/three/four/")
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("ResolveReference: %s", base.ResolveReference(u))
}
