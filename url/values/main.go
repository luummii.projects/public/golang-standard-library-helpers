package main

import (
	"fmt"
	"log"
	"net/url"
)

// Values maps a string key to a list of values. It is typically used for query parameters and form values. Unlike in the http.Header map, the keys in a Values map are case-sensitive.
func main() {
	v := url.Values{}
	v.Set("name", "Luummii")
	v.Add("friend", "Friend 1")
	v.Add("friend", "Friend 2")

	// Adds the values into form ("bar=baz&foo=quux") sorted by key.
	val := v.Encode()

	// Create a base url.
	base, err := url.Parse("http://localhost/")
	if err != nil {
		log.Fatalln(err)
	}

	// Print out the base url before adding the values as it's RawQuery.
	log.Printf("Before: %s", base)

	// Add the valued to the base url.
	base.RawQuery = val

	// Now print out the url which will show the entire url
	// with the val query appended to it.
	fmt.Printf("After: %s", base)
}
