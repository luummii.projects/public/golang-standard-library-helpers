package main

import (
	"log"
	"net/url"
)

func main() {
	// Parse a url.
	u, err := url.Parse("https://example.com/foo%2fbar")
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("RequestURI: %s", u.RequestURI())
}
