package main

import (
	"log"
	"os"
)

func main() {
	logger := log.New(os.Stdout, "Logged: ", log.Lshortfile)

	// Print out the loggers current prefix
	//
	// Prefix returns the output prefix for the logger.
	log.Println(logger.Prefix())

	logger.Println("Hello, Logger!")

	// Change the loggers prefix
	//
	// SetPrefix sets the output prefix for the logger.
	logger.SetPrefix("Log: ")

	log.Println(logger.Prefix())
	logger.Print("Hello, Logger!")
}
