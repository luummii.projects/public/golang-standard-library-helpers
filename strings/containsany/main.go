package main

import (
	"log"
	"strings"
)

func main() {
	// ContainsAny reports whether any Unicode code points in
	// chars are within s.
	//
	// false since there is no `i` in `team`
	log.Println(strings.ContainsAny("team", "i"))

	// true since `a` is in failure
	log.Println(strings.ContainsAny("failure", "abc"))
}
