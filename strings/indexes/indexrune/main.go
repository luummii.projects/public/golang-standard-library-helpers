package main

import (
	"log"
	"strings"
)

func main() {
	// IndexRune returns the index of the first instance
	// of the Unicode code point r, or -1 if rune is not present in s.
	log.Println(strings.IndexRune("Hello, World!", rune('o'))) // 4
	log.Println(strings.IndexRune("Hello, World!", rune('z'))) // -1
}
