package main

import (
	"log"
	"strings"
)

func main() {
	// IndexByte returns the index of the first instance of c in s,
	// or -1 if c is not present in s.
	log.Println(strings.IndexByte("Hello, World!", byte('o'))) // 4
	log.Println(strings.IndexByte("Hello, World!", byte('z'))) // -1
}
