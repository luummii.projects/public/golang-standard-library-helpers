package main

import (
	"log"
	"strings"
)

func main() {
	// Create a new string str
	str := "   Hello   "

	// Print out str before calling strings.Trim for comparison
	log.Println(str)

	// Trim all space from either side of the string str
	//
	// Trim returns a slice of the string s with all leading and
	// trailing Unicode code points contained in cutset removed.
	log.Println(strings.Trim(str, " "))
}
