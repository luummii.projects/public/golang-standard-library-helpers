package main

import (
	"log"
	"strings"
)

func main() {
	// TrimLeft returns a slice of the string s with all leading
	// Unicode code points contained in cutset removed.
	log.Println(strings.TrimLeft("Hello, World!", "Hello, ")) // World!

	// TrimRight returns a slice of the string s, with all trailing
	// Unicode code points contained in cutset removed.
	log.Println(strings.TrimRight("Hello, World!", ", World!")) // Hello
}
