package main

import (
	"log"
	"strings"
)

func main() {
	// ContainsRune reports whether the Unicode code point r is within s.
	log.Println(strings.ContainsRune("abc", 'a')) // true
	log.Println(strings.ContainsRune("abc", 'z')) // false
}
