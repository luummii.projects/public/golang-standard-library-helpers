package main

import (
	"log"
	"strings"
)

func main() {
	// HasSuffix tests whether the string s ends with suffix.
	log.Println(strings.HasSuffix("hello world", "world"))  // true
	log.Println(strings.HasSuffix("hello world", "worldy")) // false
	log.Println(strings.HasSuffix("hello world", ""))       // true
}
