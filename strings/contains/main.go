package main

import (
	"log"
	"strings"
)

func main() {
	a := "Hello, World!"
	b := "Hello"
	c := "Nope"

	// Contains reports whether substr is within s.
	if strings.Contains(a, b) {
		log.Printf("'%s' contains '%s'\n", a, b)
	}

	if !strings.Contains(a, c) {
		log.Printf("'%s' doesn't contain '%s'\n", a, c)
	}

	log.Println(strings.Contains("radovsky", "radov"))
	log.Println(strings.Contains("radovsky", "rados"))
}
