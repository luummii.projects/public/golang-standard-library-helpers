package main

import (
	"log"
	"strings"
)

func main() {
	// ToTitle returns a copy of the string s with all
	// Unicode letters mapped to their title case.
	log.Println(strings.ToTitle("hello, world!"))
}
