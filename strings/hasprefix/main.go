package main

import (
	"log"
	"strings"
)

func main() {
	// HasPrefix tests whether the string s begins with prefix.
	log.Println(strings.HasPrefix("hello world", "hello")) // true
	log.Println(strings.HasPrefix("hello world", "ello"))  // false
	log.Println(strings.HasPrefix("hello world", ""))      // true
}
