package main

import (
	"log"
	"strings"
)

func main() {
	// ToLower returns a copy of the string s with all
	// Unicode letters mapped to their lower case.
	log.Println(strings.ToLower("HELLO, WORLD!")) // hello, world!
}
