package main

import (
	"log"
	"strings"
)

func main() {
	// Join two strings with a comma `,` and print the newly
	// created concatenated string
	//
	// Join concatenates the elements of a to create a single
	// string. The separator string sep is placed between
	// elements in the resulting string.
	log.Println(strings.Join([]string{"Hello", "World!"}, ", "))
}
