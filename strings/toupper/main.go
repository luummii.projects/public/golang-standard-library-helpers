package main

import (
	"log"
	"strings"
)

func main() {
	// ToUpper returns a copy of the string s with all Unicode
	// letters mapped to their upper case.
	log.Println(strings.ToUpper("Gopher"))
}
