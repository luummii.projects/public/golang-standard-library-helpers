package main

import (
	"log"
	"strings"
)

func main() {
	// LastIndexByte returns the index of the last instance
	// of c in s, or -1 if c is not present in s.
	log.Println(strings.LastIndexByte("Hello, World!", byte('o'))) // 8
	log.Println(strings.LastIndexByte("Hello, World!", byte('z'))) // -1
}
