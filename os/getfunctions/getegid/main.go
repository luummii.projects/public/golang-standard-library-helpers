package main

import (
	"log"
	"os"
)

func main() {
	// Get the numeric effective group id of the caller
	//
	// Getegid returns the numeric effective group id of the caller.
	id := os.Getegid()

	// Print the id
	log.Println(id)
}
